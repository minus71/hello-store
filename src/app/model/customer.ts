export interface Customer {
	_id: string;
	code: string;
	description: string;
	creationDate?: string;
	email?: string;
	tags?: string[];
	updateDate?: string;
}