import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable, of } from 'rxjs';
import { Customer } from '../model/customer';
import { environment } from '../../environments/environment';
import { map } from 'rxjs/operators';
@Injectable({
  providedIn: 'root'
})
export class CustomerService {
  
  constructor(private http:HttpClient) { }
  
  baseURL = environment.apiURL
  
  
  getAllCustomers():Observable<Customer[]>{
    return this.http.get<Customer[]>(`${this.baseURL}/customer`);
  }
  getCustomer(id:string):Observable<Customer>{
    return this.http.get<Customer>(`${this.baseURL}/customer/${id}`);
  }
  insert(customer: Customer) {
    return this.http.post<Customer>(`${this.baseURL}/customer`,customer);
  }
  delete(id: any) : Observable<string> {
    return this.http.delete<string>(`${this.baseURL}/customer/${id}`).pipe(map(_=>id));
  }
  

}
