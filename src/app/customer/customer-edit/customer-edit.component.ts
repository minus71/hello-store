import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { Store } from '@ngrx/store';
import { AppState } from 'src/app/app.module';
import { Customer } from 'src/app/model/customer';
import { CustomerService } from 'src/app/service/customer.service';
import { insertCustomer } from '../customer.actions';

@Component({
  selector: 'app-customer-edit',
  templateUrl: './customer-edit.component.html',
  styleUrls: ['./customer-edit.component.scss']
})
export class CustomerEditComponent implements OnInit {

  constructor(private store:Store<AppState>, private customerService:CustomerService) { 
    this.customer={
      _id:"",
      code:"",
      description:"",
      
    }
  }
  customer:Customer


  ngOnInit(): void {
  }
  validate(){
    console.log("Validating ",this.customer)
    
    if(true){
      this.customerService.insert(this.customer).subscribe(res=>{
        this.store.dispatch(insertCustomer({customer:res}));
        // this.router.navigate(["customer",res._id]);
      },err=>{
        console.log("Error saving customer: ",err.error.error)
      });
    }


    return false;
  }

}
