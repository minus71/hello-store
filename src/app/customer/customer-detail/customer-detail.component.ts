import { ChangeDetectionStrategy, Component, OnDestroy, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { Store } from '@ngrx/store';
import { Observable, Subject } from 'rxjs';
import { map, shareReplay, tap , finalize, takeUntil } from 'rxjs/operators';
import { Customer } from 'src/app/model/customer';
import { CustomerService } from 'src/app/service/customer.service';
import { deleteCustomer, loadCustomer } from '../customer.actions';
import { CustomerState } from '../customer.reducer';

@Component({
  selector: 'app-customer-detail',
  templateUrl: './customer-detail.component.html',
  styleUrls: ['./customer-detail.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class CustomerDetailComponent implements OnInit , OnDestroy{
  
  constructor(private router:Router ,private route:ActivatedRoute ,private store:Store<{customer:CustomerState}>, private customerService:CustomerService) {}
  
  customer$: Observable<Customer>

  // id$: Observable<string>

  id:string

  ngOnDestroy(): void {
    this._unsubscribeAll.next();
    this._unsubscribeAll.complete();
  }
  _unsubscribeAll = new Subject<any>(); 



  ngOnInit(): void {
    this.route.params.pipe(
      map(pms=>pms.id),
      tap(id=>this.id = id),
      tap(id=> this.store.dispatch(loadCustomer({_id:id})) ),
      takeUntil(this._unsubscribeAll),
    ).subscribe();


    this.customer$ = this.store.select("customer","customer");
    
  }

  delete(id){
    this.store.dispatch(deleteCustomer(id));



    this.customerService.delete(id).subscribe(res=>{
      this.router.navigate(["customer"]);
    },err=>{
      console.log("Error",err.error.error);
    });
  }

}
