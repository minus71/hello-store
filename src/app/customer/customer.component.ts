import { Component, OnInit } from '@angular/core';
import { Store } from '@ngrx/store';
import { loadCustomers } from './customer.actions';
import { CustomerState } from './customer.reducer';

@Component({
  selector: 'app-customer',
  templateUrl: './customer.component.html',
  styleUrls: ['./customer.component.scss']
})
export class CustomerComponent implements OnInit {

  constructor(private store:Store<CustomerState>) { }

  ngOnInit(): void {
    console.log("Init customer comp");
    
  }

}
