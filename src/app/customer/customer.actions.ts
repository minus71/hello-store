import { Action } from '@ngrx/store';
import { createAction, props } from '@ngrx/store';

import { Customer } from '../model/customer';

export const loadCustomers = createAction(
  '[Customer] Load Customers' 
);

export const setCustomers = createAction(
  '[Customer] Set Customers' , props<{customers:Customer[]}>()
);

export const loadCustomer = createAction(
  '[Customer] Load Customer' , props<{_id:string}>()
);
export const deleteCustomer = createAction(
  '[Customer] Delete Customer' , props<{_id:string}>()
);
export const unsetCustomer = createAction(
  '[Customer] Unset Customer' , props<{_id:string}>()
);

export const setCustomer = createAction(
  '[Customer] Set Customer' , props<{customer:Customer}>()
);

export const insertCustomer = createAction(
  '[Customer] Insert Customer' , props<{customer:Customer}>()
);
 
export class InsertCustomerAction implements Action {
  readonly type="[Customer] Insert Customer"
  constructor(public readonly customer: Customer){}
}

const AAAInsert = new InsertCustomerAction({
  _id:"",
  code: "AAS",
  description: "bbb",
})