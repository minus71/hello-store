import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { Store } from '@ngrx/store';
import { Observable } from 'rxjs';
import { take, tap } from 'rxjs/operators';
import { CustomerService } from 'src/app/service/customer.service';
import { loadCustomers } from '../customer.actions';
import  * as fromCustomer  from '../customer.reducer';

@Component({
  selector: 'app-customer-list',
  templateUrl: './customer-list.component.html',
  styleUrls: ['./customer-list.component.scss']
})
export class CustomerListComponent implements OnInit {

  constructor(private router:Router, private store:Store<{ customer: fromCustomer.CustomerState }>) { }

  customers$:Observable<any[]>
  stateId$:Observable<any>
  ngOnInit(): void {

    this.customers$ = this.store.select("customer","customers");
    
    // this.customers$ = this.cs.getAllCustomers().pipe(tap(c=>console.log(c)));
    // this.cs.getAllCustomers().subscribe(cs=>this.store.dispatch(loadCustomers()))

    console.log("Init customer comp");
    this.store.select("customer","lastUpdate").pipe(
      take(1)
    ).subscribe(lastUpdate=>{
      if(!lastUpdate){
        this.store.dispatch(loadCustomers());
      } else {
        console.log(lastUpdate)
        const delta = new Date().getTime() - lastUpdate.getTime() ;
        console.log(delta)
        if(delta > 1000 * 30) {
          this.store.dispatch(loadCustomers());
        }
      }
    })
    this.stateId$ = this.store.select("customer","stateId").pipe(tap(s=>console.log(s)));
  }

  new(){
    this.router.navigate(["customer","new"])
  }



}
