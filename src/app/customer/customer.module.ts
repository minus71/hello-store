import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { CustomerRoutingModule } from './customer-routing.module';
import { CustomerComponent } from './customer.component';
import { CustomerListComponent } from './customer-list/customer-list.component';
import { CustomerDetailComponent } from './customer-detail/customer-detail.component';
import { StoreModule } from '@ngrx/store';
import * as fromCustomer from './customer.reducer';
import { EffectsModule } from '@ngrx/effects';
import { CustomerServiceEffects } from './customer-service.effects';
import { CustomerEditComponent } from './customer-edit/customer-edit.component';
import { SharedModule } from '../shared/shared.module';


@NgModule({
  declarations: [
    CustomerComponent,
    CustomerListComponent,
    CustomerDetailComponent,
    CustomerEditComponent
  ],
  imports: [
    CommonModule,
    CustomerRoutingModule,
    StoreModule.forFeature(fromCustomer.customerFeatureKey, fromCustomer.reducer),
    EffectsModule.forFeature([CustomerServiceEffects]),
    SharedModule,
  ]
})
export class CustomerModule { }
