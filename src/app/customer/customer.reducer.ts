import { Action, createReducer, on } from '@ngrx/store';
import { Customer } from '../model/customer';
import * as CustomerActions from './customer.actions';

export const customerFeatureKey = 'customer';

export interface CustomerState {
  stateId: "Customer"
  customers: Customer[]
  customer?: Customer,
  lastUpdate?: Date,
}

export const initialState: CustomerState = {
  stateId: "Customer",
  customers: [],
};


export const reducer = createReducer(
  initialState,

  // on(CustomerActions.loadCustomers, (state) => state),
  on(CustomerActions.setCustomers, (state, { customers }) => setAll(state, customers)),
  on(CustomerActions.setCustomer, (state, { customer }) => setOne(state, customer)),
  on(CustomerActions.loadCustomer, (state, {}) => ({...state,customer:null})),
  on(CustomerActions.unsetCustomer, (state, {_id}) => ({...state,customers:state.customers.filter(c=>c._id!=_id)})),
  on(CustomerActions.insertCustomer,(state,{customer})=> ({...state, customers:[...state.customers, customer] }) )

);

function setAll(state: CustomerState, customers: Customer[]): CustomerState {
  return { ...state, customers, lastUpdate: new Date() };
}
function setOne(state: CustomerState, customer: Customer): CustomerState {
  return { ...state, customer };
}