import { Injectable } from '@angular/core';
import { Router } from '@angular/router';
import { Actions, createEffect, ofType } from '@ngrx/effects';
import { Action, createAction, props } from '@ngrx/store';
import { EMPTY, Observable, of } from 'rxjs';
import { catchError, map, mergeMap, tap } from 'rxjs/operators';
import { CustomerService } from '../service/customer.service';
import { loadCustomer, loadCustomers, setCustomers , setCustomer, deleteCustomer, unsetCustomer, insertCustomer} from './customer.actions';



@Injectable()
export class CustomerServiceEffects {



  constructor(
    private actions$: Actions, 
    private customerService: CustomerService,
    private router: Router) {
  }
  loadCustomers$ = createEffect(() => this.actions$.pipe(
    ofType(loadCustomers),
    // tap(a=>console.log(a)),
    mergeMap((action) => wrapExchange(action,()=>getCustomers(this.customerService)),
    ))
  )
  loadCustomer$ = createEffect(() => this.actions$.pipe(
    ofType(loadCustomer),
    // tap(a=>console.log(a)),
    mergeMap((action) => wrapExchange(action, ()=>getCustomer(this.customerService,action._id)),
    ))
  )
  insertCustomer$ = createEffect(() => this.actions$.pipe(
    ofType(insertCustomer),
       mergeMap((action) => wrapExchange((action), () => {this.router.navigate(["customer",action.customer._id]); return EMPTY}),
    ))
  )
  deleteCustomer$ = createEffect(() => this.actions$.pipe(
    ofType(deleteCustomer),
    mergeMap((action) => wrapExchange(action, ()=>this.customerService.delete(action._id).pipe(
      map((_id)=>unsetCustomer({_id}))
    )),
    ))
  )

  

}


function getCustomers(customerService: CustomerService): Observable<Action> {

  return customerService.getAllCustomers().pipe( 
    map(res=>setCustomers({customers:res}))
  )

  
}
function getCustomer(customerService: CustomerService,id:string): Observable<Action> {

  return customerService.getCustomer(id).pipe( 
    map(res=>setCustomer({customer:res}))
  )
}
export interface ServiceExchange {
  request:Action,
  response: Action,
}

function wrapExchange(request:Action, fn:()=>Observable<Action>){
  return fn().pipe(
    catchError(e => of(serviceFailed({ action:request, err: e })))
  )
}

const serviceFailed = createAction("[Effect] Service call failed", props<{ action: Action, err: Error }>());