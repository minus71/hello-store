import { TestBed } from '@angular/core/testing';
import { provideMockActions } from '@ngrx/effects/testing';
import { Observable } from 'rxjs';

import { CustomerServiceEffects } from './customer-service.effects';

describe('CustomerServiceEffects', () => {
  let actions$: Observable<any>;
  let effects: CustomerServiceEffects;

  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [
        CustomerServiceEffects,
        provideMockActions(() => actions$)
      ]
    });

    effects = TestBed.inject(CustomerServiceEffects);
  });

  it('should be created', () => {
    expect(effects).toBeTruthy();
  });
});
